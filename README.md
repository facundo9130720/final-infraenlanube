Formatear y montar un volumen en un directorio de nuestra instancia EC2 (en nuestro caso, lo haremos en /var/lib/docker/volumes/ )
1. Crear instancia ec2 en amazon web service
Ingresar a https://aws.amazon.com/es/ 
Crear instancia ec2
configurar la misma a gusto personal 
lanzar instancia y conectarse a la misma 
Ingresar contraseña de usuario con el comando: 
sudo passwd ubuntu 
Ingresar contraseña de “root” con el comando:
sudo passwd root 
Actualizar el sistema con el comando:
sudo apt update && sudo apt upgrade 
1. formatear y montar volumen
elegir uno de los siguientes volúmenes de nuestra instancia EC2 con el siguiente comando(en nuestro caso, usamos "xvdz" que es un volumen que hemos creado durante el desarrollo de nuestra instancia EC2, podemos crearlo durante la creación de nuestra instancia EC2 o yendo al apartado “volúmenes”)
lsblk
NAME 	MAJ:MIN   RM  SIZE RO TYPE MOUNTPOINTS
loop0  	7:0  	0 25.2M  1 loop /snap/amazon-ssm-agent/7993
loop1  	7:1  	0 55.7M  1 loop /snap/core18/2829
loop2  	7:2  	0 38.8M  1 loop /snap/snapd/21759
xvda 	202:0  	0	8G  0 disk
├─xvda1  202:1  	0	7G  0 part /
├─xvda14 202:14 	0	4M  0 part
├─xvda15 202:15 	0  106M  0 part /boot/efi
└─xvda16 259:0  	0  913M  0 part /boot
xvdbz	202:19712  0   10G  0 disk
Formateamos el volumen con el siguiente comando:
sudo mkfs.xfs /dev/xvdbz
obtener uuid de volumen con el siguiente comando:
sudo blkid /dev/xvdbz
/dev/xvdbz: UUID="237978ac-5f22-46da-9ac1-a403a9802c99" BLOCK_SIZE="512" TYPE="xfs"
Crear directorio del montaje a elección personal, en nuestro caso, vamos a crear el directorio “final-infra” con el siguiente comando:
sudo mkdir /var/lib/docker/volumes/final-infra
Crear el archivo de unidad de montaje en systemd con vim, usando el siguiente comando: 
sudo vim /etc/systemd/system/mnt-final-infra.mount
[Unit]
Description=/var/lib/docker/volumes/final-infra
After=network.target
[Mount]
What=UUID=237978ac-5f22-46da-9ac1-a403a9802c99
Where=/var/lib/docker/volumes/final-infra
Type=xfs
Options=defaults
[Install]
WantedBy=multi-user.target
wr
Para activar, montar y verificar el punto de montaje, usaremos los siguientes comandos: 
sudo systemctl daemon-reload
systemctl enable --now mnt-final-infra.mount
sudo systemctl status mnt-final-infra.mount

Crear y clonar un repositorio git con las ramas 'main' y 'develop
1.Crear repositorio git 
Ingresar a https://gitlab.com/users/sign_in
Crear un “new project”, yendo a la opción “create blank project”
Realizar configuración a gusto personal 
Crear llaves en instancia EC2 y ponerlas en el apartado ssh keys de gitlab
Ir a instancia EC2 
Clonar repositorio con el siguiente comando(En nuestro caso, nuestro repositorio es “final-infraenlanube”):
git clone https://gitlab.com/facundo9130720/final-infraenlanube.git
Ingresar al repositorio con el siguiente comando:
     cd tp-dockerfile
Crear la rama ‘develop’ y guardarla en el repositorio git remoto con el siguiente comando(la rama main ya viene predeterminada en nuestro repositorio):
git checkout -b develop
git add . 
git commit -m “Creación de rama develop” 
git push -u origin develop
Cambiar de ramas con el siguiente comando:
git checkout main
git checkout develop 
(Ubicarnos en la rama main para realizar este trabajo)
Crear un dockerfile donde ejecutaremos la configuración para lanzar un contenedor usando la imagen 'ubuntu/apache2' montando el directorio '/var/www/html' con el directorio asociado al volumen montado
1.Crear dockerfile
Ingresamos nuevamente a nuestro repositorio git con el siguiente comando:
cd final-infraenlanube
creamos el archivo dockerfile con el siguiente comando:
vim dockerfile 
2.Crear archivos index.html, nginx.conf y configurarlos 
para incluir los archivos index.html(el cual servirá para visualizar nuestro contenedor docker en nuestro navegador) y nginx.conf (para optimizar varios aspectos del sitio web de nuestro contenedor) que se ejecutarán en nuestro dockerfile, los crearemos por separado en nuestro repositorio git, de la siguiente manera:
vim index.html 
Dentro del fichero index.html, ponemos lo siguiente y guardamos con “wr”: 
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mi App</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #e8f5e9;
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }
        .container {
            text-align: center;
            background-color: #ffffff;
            padding: 2rem;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        h1 {
            color: #388e3c;
        }
        p {
            color: #66bb6a;
        }
        .button {
            display: inline-block;
            padding: 10px 20px;
            margin-top: 20px;
            color: #ffffff;
            background-color: #388e3c;
            border-radius: 5px;
            text-decoration: none;
            transition: background-color 0.3s ease;
        }
        .button:hover {
            background-color: #2e7d32;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Bienvenido</h1>
        <p>Esta es una aplicación de ejemplo creada con Docker y Apache2 para el final de infra en la nube.</p>
        <a href="#" class="button">Empezar</a>
    </div>
</body>
</html>
vim nginx.conf
Dentro del fichero index.html, ponemos lo siguiente y guardamos con “wr” (es importante tener nginx instalado y configurado en nuestra instancia EC2): 
server {
    listen 80;
    server_name finalinfra.com;

    location / {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_pass http://ip_docker_server:8000;
    }
}
3.configurar dockerfile
habiendo hecho los pasos anteriores, vamos a configurar el dockerfile, para ello, ingresamos por editor de texto vim y añadimos lo siguiente: 
vim dockerfile
FROM ubuntu

RUN apt update && apt install apache2 git -y

RUN apt-get install -y nginx

COPY index.html /var/www/html/index.html

COPY nginx.conf /etc/nginx/sites-available/default

EXPOSE 80

CMD apachectl D FOREGROUND & nginx -g 'daemon off;"
4.Construir imagen 
Una vez hecho el dockerfile, vamos a crear la imagen que usará la configuración del dockerfile y la misma la vamos a lanzar en el directorio asociado al volumen que formateamos y montamos al principio, estos pasos, los haremos con los siguientes comandos:
docker build -t final-infraenlanube
sudo docker run -d -p 80:80 -v /var/lib/docker/volumes/final-infra:/var/www/html final-infraenlanube
.
