FROM ubuntu 

RUN apt update && apt install apache2 git -y 

RUN apt-get install -y nginx

COPY index.html /var/www/html/index.html 

COPY nginx.conf /etc/nginx/sites-available/default 

EXPOSE 80 

CMD apachectl D FOREGROUND & nginx -g 'daemon off;"

